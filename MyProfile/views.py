from django.shortcuts import render, redirect
from .forms import TaskForm
from .models import Task
from . import forms
from datetime import datetime, date

# Create your views here.
def home(request):
    return render(request, "Home.html")

def misc(request):
    return render(request, "Misc.html")

def createtask(request):
    if request.method=='POST':
        form=forms.TaskForm(request.POST)
        if form.is_valid():
            time = form.cleaned_data['time']
            day = time.strftime("%A")
            kategori=form.cleaned_data['kategori']
            task=form.cleaned_data['task']
            tempat=form.cleaned_data['tempat']
            hour=form.cleaned_data['hour']
            task = Task(time=time, day=day, kategori=kategori, task=task, tempat=tempat, hour=hour)
            task.save()
            #Task.objects.all().delete()
            return redirect('list')
        else:
            return render(request, "CreateTask.html",{'form':form})
    form=forms.TaskForm()
    return render(request, "CreateTask.html",{'form':form})

def tasklist(request):
    forms=Task.objects.all().order_by('date')
    return render(request, "TaskList.html",{'forms':forms})

def taskRemove(request, task_id):
    task=Task.objects.get(id=task_id)
    task.delete()
    return redirect('list')

# def all_persons(request):
#     persons=Person.objects.all()
#     contact={'all_persons':persons}
#     return render(request, 'all-persons.html')